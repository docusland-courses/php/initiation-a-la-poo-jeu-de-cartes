# Exercice de Programmation Orientée Objet : Un jeu de cartes

La programmation orientée objet (POO) est un paradigme au sein de la programmation informatique. Il s’agit d’une représentation des choses, un modèle cohérent – partagé à travers différents langages qui permettent son usage (Python, Java, C++).Le but de la POO consiste à définir et faire interagir entre eux des objets, compris ici comme tous types de structures issues d’un langage donné.

Ici, afin de s'approprier le concept de la POO, nous allons implémenter des jeux de cartes. 

## Contexte
En Europe, les jeux de cartes ont commencé à être utilisés dans la seconde moitié du XIVe siècle, en provenance des civilisations orientales. Ils nous seraient parvenus via les Mamelouks, ancienne milice égyptienne. Selon plusieurs documents cités dans le livre réalisé par le musée de Cluny et l'encyclopédie Universalis, ce type de jeux a d'ailleurs été décrié dès son arrivée par l'Église, qui était contre les jeux de hasar

La carte à jouer est la base du jeu de cartes. Suivant les régions et les époques, les cartes, figures et couleurs peuvent différer : par exemple, un jeu de cartes français possède des figures nommées avec les symboles de couleur: cœur, carreau, pique, trèfle

## Attendus de l'exercice

Nous allons implémenter ici deux jeux de cartes classiques : un jeu de 32 cartes et un jeu de 52 cartes.

```mermaid
classDiagram
    Carte --* AbstractJeuDeCartes
    AbstractJeuDeCartes <|-- Jeu32Cartes
    AbstractJeuDeCartes <|-- Jeu52Cartes

    class Carte{
      -int rang
      -str valeur
      -str couleur
      + __construct(rang, valeur, couleur)
      + estPlusGrandQue(Carte carte) bool
      + estPlusPetitQue(Carte carte) bool
      + estEgal(Carte carte)bool
    }
    class AbstractJeuDeCartes{
      +string couleurs[0..*]
      +string valeurs[0..*]
      +Carte tas[0..*] 
      +__construct()
      +melanger()
      +pioche()
    }
    class Jeu52Cartes{
        +string couleurs[0..*]
        +string valeurs[0..*] 
    }
    class Jeu32Cartes{
        +string couleurs[0..*]
        +string valeurs[0..*] 
    }
```


## Exercice bonus
Vous connaissez le jeu de la bataille ? Ce jeu se joue à deux. 
Simultanément chaque joueur tire la première carte de sa main. 
Si une carte est plus forte que l'autre, le joueur l'ayant posée récupères les cartes posées sur la table et les rajoute à sa main. 
Si les cartes sont de valeur égales, les joueurs posent une carte qu'on ne comparera pas, puis une autre carte qu'on compare avec l'adverse. Celui qui a posé la carte la plus forte emporte toutes les cartes présentes sur la table. 
Pour celà, voici une ébauche de diagramme de classe à réaliser. 

```mermaid
classDiagram
    Bataille *-- Joueur

    class Bataille{
      -Carte table[0..*]
      +Joueur joueurs[2]
      + __construct(int rang, string valeur, string couleur)
      + distributionCartes(Jeu32Cartes cartes)
      + joueUneManche()
      + estFini()bool
      +__toString()str
    }
    class Joueur{
      +string nom
      +Carte main[0..*]
      +__construct(string nom)
      +joueUneCarte()
      + mainVide()bool
      +__toString()str
    }
```